from preprocessData.connection import closeConnection
import csv
# import psycopg2
from preprocessData.getPath import getPath
import preprocessData.config as cfg
# from sqlalchemy import *

path = getPath(cfg.paths['orga'])
rcn = 0
projectID = 1
id = 4
role = 3
name = 5
shortname = 6
contribution = 9
country = 10
street = 11
city = 12
postcode = 13

def processOrganizations(connection):

    # cursor = connection.cursor()
    # cursor.execute("drop table if exists organisations;")
    # cursor.execute("create table if not exists organisations\
    #     (orgaid int not null,\
    #     orgarole text,\
    #     organame text,\
    #     contribution int,\
    #     rcn int,\
    #     constraint organisations_pk\
    #     primary key (orgaid, rcn));")

    with open(path, 'r') as csvFile:
        reader = csv.reader(csvFile, delimiter=",")
        for row in reader:

            if row[rcn].isalpha() or row[contribution] == "":
                continue

            values = {}
            values['orgaid'] = row[id]
            values['orgarole'] = row[role].replace('\'', '*')
            values['organame'] = row[name].replace('\'', '*')
            try:
                values['contribution'] = float(row[contribution].replace(",", "."))
            except ValueError:
                values['contribution'] = 0

            values['rcn'] = row[0]

            query = "insert into organisations (orgaid, orgarole, organame, contribution, rcn) values ({orgaid}, \'{orgarole}\', \'{organame}\', {contribution}, {rcn})".format(
                **values)
            # print(query)

            try:
                connection.execute(query)
                connection.commit()

            except Exception:
                connection.rollback()
                # print("fehler eingetreten")


    print("organisations' data successfully processed")
    closeConnection(connection)
