import os

def getPath(relativePath):
    dirpath = os.getcwd().split('src')
    path = os.path.join(dirpath[0], relativePath)
    return path
