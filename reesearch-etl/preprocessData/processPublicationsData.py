from preprocessData.connection import closeConnection
# import psycopg2
import csv
from preprocessData.getPath import getPath
import preprocessData.config as cfg
# import sqlalchemy

path = getPath(cfg.paths['publications'])
rcn = 0
title = 1
authors = 6
journal = 7
journalNumber = 8
year = 9
issn = 11
doi = 12

def processPublications(connection):

    # cursor = connection.cursor()
    # cursor.execute("drop table if exists publications;")
    # cursor.execute("create table publications\
    #     (rcn int,\
    #     title text,\
    #     authors text,\
    #     journal text,\
    #     issn text,\
    #     doi text,\
    #     year int,\
    #     constraint publications_pk\
    #             primary key (issn, rcn));")

    with open(path, 'r') as csvFile:
        reader = csv.reader(csvFile, delimiter=",")
        for row in reader:

            if row[rcn].isalpha() or row[issn] == '':
                continue

            values = {'rcn': row[rcn], 'title': row[title].replace('\'', '*'),
                      'authors': row[authors].replace('\'', '*'), 'journal': row[journal].replace("\'", "*"),
                      'issn': row[issn], 'doi': row[doi]}

            query = "insert into publications (rcn, title, authors, journal, issn, doi) values ({rcn}, \'{title}\', \'{authors}\', \'{journal}\', \'{issn}\', \'{doi}\')".format(
                **values)
            # print(query)

            try:
                connection.execute(query)
                connection.commit()
            except Exception:
                connection.rollback()
                # print("unique violation")


    print("publications data successfully processed")
    closeConnection(connection)

