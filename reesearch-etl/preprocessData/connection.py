# import psycopg2
# import preprocessData.config as cfg
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

# postgres = 'postgresql://postgres:example@postgres01.cordis_search_reesearchnet:5432/postgres'
postgres = 'postgresql://postgres:example@localhost:5432/postgres'

#TODO: create config file with secure pw!
def getConnection():
    engine = create_engine(postgres)
    Session = sessionmaker(bind=engine)
    session = Session()
    return session

def closeConnection(connection):
    connection.close()