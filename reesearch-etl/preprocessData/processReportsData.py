import csv
from preprocessData.connection import getConnection
from preprocessData.getPath import getPath
import preprocessData.config as cfg

path = getPath(cfg.paths['reports'])
rcn = 0
title = 2
summary = 4
workPerformed = 5
results = 6
projectID = 9
acronym = 10

def processReportsData(connection):
    # cursor = connection.cursor()
    # cursor.execute("drop table if exists reports;")
    # cursor.execute("create table if not exists reports(rcn int primary key,title text, summary text, workperformed text, result text, projectid int, acronym varchar(255));")

    with open(path, 'r') as csvFile:
        reader = csv.reader(csvFile, delimiter=",")
        # data = {}
        for row in reader:
            if row[rcn].isalpha() or row[projectID].find(":") != -1 or row[projectID] == '':
                continue

            values= {}
            values['rcn'] = row[rcn]
            values['title'] = row[title].replace('\'', '*')
            values['summary'] = row[summary].replace('\'', '*')
            values['work'] = row[workPerformed].replace('\'', '*')
            values['result'] = row[results].replace('\'', '*')
            values['projectid'] = row[projectID]
            values['acronym'] = row[acronym].replace('\'', '*')

            query = "insert into reports (rcn, title, summary, workperformed, result, projectid, acronym) values ({rcn}, \'{title}\', \'{summary}\', \'{work}\', \'{result}\', {projectid}, \'{acronym}\')".format(**values)      # values = (row[rcn], row[title], row[summary], row[workPerformed], row[results], row[projectID], row[acronym])
            # print(query)
            try :
                connection.execute(query)
                connection.commit()
            except Exception:
                connection.rollback()
            # data[rcn] = row[title] +  row[summary] + row[workPerformed] + row[results]

        # connection.commit()
        print("reports' data successfully procesed")
        # return data

# def getConnection():
#     conn = psycopg2.connect(database="postgres", user="postgres", password="test123", host="127.0.0.1", port="5432")
#     print("Connection with DB established")
#     return conn



# data = processReportsData()
