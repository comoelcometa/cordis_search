mysql = {'host': '127.0.0.1',
         'user': 'postgres',
         'passwd': 'example',
         'db': 'postgres',
         'port': 5432}

paths = {'orga': 'reesearch-etl/preprocessData/cordis-h2020organizations.csv',
         'publications': 'reesearch-etl/preprocessData/cordis-h2020projectPublications.csv',
         'reports': 'reesearch-etl/preprocessData/cordis-h2020reportsUpdated.csv',
         'mappings': 'reesearch-etl/mappings.json'
         }
