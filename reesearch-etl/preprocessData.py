from preprocessData.connection import getConnection, closeConnection
from preprocessData.processReportsData import processReportsData
from preprocessData.processOrgaData import processOrganizations
from preprocessData.processPublicationsData import processPublications


def preprocessData():
    connection = getConnection()
    # print('connected')
    processReportsData(connection)
    processOrganizations(connection)
    processPublications(connection)
    closeConnection(connection)

preprocessData()