from preprocessData.connection import getConnection
from elasticsearch import Elasticsearch
from elasticsearch.helpers import bulk
from preprocessData.processPublicationsData import processPublications
from preprocessData.processOrgaData import processOrganizations
from preprocessData.processReportsData import processReportsData
from preprocessData.getPath import getPath
import preprocessData.config as cfg
import json
rcn = 0
title = 1
summary = 2
workPerformed = 3
results = 4
acronym = 6

def getConsolidatedData(connection):
    allProjects = connection.execute("select * from reports")
    for project in allProjects:
        # body = project[1] + project[2] + project[3] + project[4]
        rcn = project[0]

        allOrganisations = connection.execute("select organame from organisations where rcn = {}".format(rcn))

        orgas = ""
        for orga in allOrganisations:
            orgas += "{}, ".format(orga[0])

        # includes titles of publications by project to the body search
        allPublications = connection.execute("select title from publications where rcn = {}".format(rcn))

        publications = ""
        for pub in allPublications:
            publications += "{}, ".format(pub[0])


        yield {
            "_index": "reports",
            "_type": "_doc",
            "_id": rcn,
            "_source": {
                "project": rcn,
                "title": project[title],
                "summary": project[summary],
                "work performed": project[workPerformed],
                "results": project[results],
                "acronym": project[acronym],
                "organisations involved": orgas,
                "publications": publications
            }
        }
def defineIndex(elasticsearch):
    path = getPath(cfg.paths['mappings'])
    with open(path, 'r') as mappings:
        body = json.load(mappings)
        elasticsearch.indices.create(index='reports', body= body)
        # print(body)


def indexData():
    connection = getConnection()
    print("indexing data...")
    es = Elasticsearch()
    defineIndex(es)
    bulk(es, getConsolidatedData(connection))
    connection.close()


indexData()
