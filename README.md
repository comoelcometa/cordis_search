# Cordis Search - The EU-funded research search engine

## How to use
### Pre-requisites
You need an up-to-date operating system which is able to run or emulate a Linux kernel.

- installed docker

  ```
  sudo snap install docker
  sudo docker run hello-world 
  ```

  (The last command runs a check)

- installed docker-compose

  ```

  sudo apt install python3-pip 
  sudo pip3 install docker-compose

  ```

- installed webbrowser

  ```
  sudo apt-get install firefox language-pack-de-base 
  ```

- installed npm

  ```
  sudo apt-get install npm 
  ```

- installed git
  
  ```
  sudo apt-get install git 
  ```
  

### Clone the repository

```
git clone https://gitlab.com/comoelcometa/cordis_search.git
```

### (Re-) build all custom images
```
make clean
make build
```

If your current user is not a member of the group 'docker' you have to run this command with sudo.

### Start all necessary container
```
make deploy
```

If your current user is not a member of the group 'docker' you have to run this command with sudo.

### Import data
```
make preprocess_data
make import
```

### Open Frontend
Browse to the website 127.0.0.1:80 with you prefered webbrowser.



## Other Functionalities

If you already imported the data to the database and only want to index it again (for instance, when trying the optimized versions), use:

```
make import
```
NOTE: if you try to run this command too shortly after deploying it might fail. Please try again a couple of seconds later.





To delete en existing index, you can run:	

```
make delete
```

(This is usually not needed when running `make import`)



If you have given some relevance feedback and you want to store it on yout computer, please run:

```
make export
```



If you want to assess the preformance of the SE, you can either use the UI (eval-mode -> evaluation), or use the command:

```
make evaluate
```

to print the results to the command line.
NOTE: if you try to run this command too shortly after deploying it might fail. Please try again a couple of seconds later.




If you are working with opt2, you cann preprocess and import the topic models data to the database using:

```
make add_topic_models_data
```

(This feature is only available in opt2!)

## How to get to work the different versions of the SE

Assuming you already imported yout data to the postgres database running `make preprocess_data` once, you can run the different versions using the following commands.

#### Baseline 

```
git checkout baseline
make clean
make build
make deploy
make import
```

#### Opt1

```
git checkout opt1
make clean
make build
make deploy
make import
```

#### Opt2

```
git checkout opt2
make clean
make build
make deploy
make add_topic_models_data 			#this takes a bit
make import
```



## Data sources

URL: https://data.europa.eu/euodp/en/data/dataset/cordisH2020projects

### organizations (maybe xlsx cause csv maybe corrupted)
http://cordis.europa.eu/data/cordis-h2020organizations.csv
id: id (origanisationID from researchers)
fields: projectID, role, name, shortname, ecContribution, country, street, city, postcode, organizationUrl

### projects
http://cordis.europa.eu/data/cordis-h2020projects.csv
id: id
fields: acronym, status, programme, topics, title, startDate, endDate, url, objective, totalCosts, ecMaxContribution

### publications
http://cordis.europa.eu/data/cordis-h2020projectPublications.csv
id: projectID
fields: title, topics authors, journalTitle, journalNumber, publishedYear, issn, doi


Maybe: project XML files (later)

## Some development hints
### frontent with vue.js
### Installation

#### Install node.js via NVM (Node Version Manager)
https://github.com/nvm-sh/nvm#install--update-script

Don't use the version of node.js provided by your package manager at Linux.

Type in your console to install a specific version of Node.js (current LTS v10.16.0)
If you got 'nvm: command not found' then restart your terminal.
```
wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash
nvm list
nvm ls-remote
nvm install <version>
nvm use <version>
nvm alias default <version>
node -v
npm install -g npm
npm -v
```


#### Install vue.js CLI (command line interface)
https://www.vuemastery.com/courses/real-world-vue-js/vue-cli

Please assure that you have installed a Node.js version above 8.

```
npm i -g @vue/cli
```

#### nice node modules for using with vue
axios (requesting APIs)
go to your frontend project and run

```
npm install axios --save
```

### backend with Python Flask
#### good source howto implement features from the flask framework 
https://blog.luisrei.com/articles/flaskrest.html

#### cross origin access
If you request the backend from outside the current network (docker) the browser will block
the cross-origin access. Add this header to your endpoint to allow it.
https://stackoverflow.com/a/45818284
```
@blueprint.after_request # blueprint can also be app~~
def after_request(response):
    header = response.headers
    header['Access-Control-Allow-Origin'] = '*'
    return response
```
