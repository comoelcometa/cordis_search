create table if not exists reports
(
	rcn integer not null
		constraint reports_pkey
			primary key,
	title text,
	summary text,
	workperformed text,
	result text,
	projectid integer,
	acronym varchar(255)
);

alter table reports owner to postgres;

create table if not exists organisations
(
	orgaid integer not null,
	orgarole text,
	organame text,
	contribution integer,
	rcn integer not null,
	constraint organisations_pk
		primary key (orgaid, rcn)
);

alter table organisations owner to postgres;

create table if not exists publications
(
	rcn integer not null,
	title text,
	authors text,
	journal text,
	issn text not null,
	doi text,
	constraint publications_pk
		primary key (issn, rcn)
);

alter table publications owner to postgres;

create table if not exists relevant
(
	topic_id varchar not null,
	rcn integer not null,
	constraint relevant_pkey
		primary key (topic_id, rcn)
);

alter table relevant owner to postgres;

create table if not exists evaluations
(
	session integer not null,
	version integer,
	topic_id varchar not null,
	rcn integer not null,
	constraint evaluations_pkey
		primary key (session, topic_id, rcn)
);

alter table evaluations owner to postgres;

