import requests
import sqlalchemy as db
from sqlalchemy import create_engine
from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.dialects.postgresql import ARRAY
import json

engine = create_engine('postgresql://postgres:example@172.16.238.3:5432/postgres')

Base = declarative_base()

class Reports(Base):
   __tablename__ = 'reports'
   rcn = Column(Integer, primary_key =  True)
   title = Column(String)
   summary = Column(String)
   workperformed = Column(String)
   result = Column(String)
   projectid = Column(Integer)
   acronym = Column(String)



# connection = engine.connect()
# metadata = db.MetaData()
# reports = db.Table('reports', metadata, autoload=True, autoload_with=engine)

# print(reports.columns.keys())

Session = sessionmaker(bind=engine)
session = Session()
# query = session.query(Reports).filter(Reports.projectid.contains([205060, 263692])).all()

payLoad = dict(q = "fraunhofer")
requestElastic = requests.get('http://172.16.238.2:9200/_search',params=payLoad)

projectIds = []
for item in requestElastic.json()['hits']['hits']:
    # projectIds.append(int(item['_source']['_id']))
    projectIds.append(item['_source']['project'])

print(projectIds)

query = session.query(Reports).filter(Reports.rcn.in_(projectIds)).all()
# query= session.query(Reports).all()

results = []
for row in query:
    print('ProjektID: ', row.projectid, 'Titel: ', row.title)
    results.append({'index':row.projectid,'text':row.title})

data = {'results': results}
js = json.dumps(data)

print(js)
# data = requestElastic.json()['hits']['hits']



