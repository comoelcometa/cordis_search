from sqlalchemy import create_engine, Column, String, Integer
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from resources import getEvaluationTopics, getQuery, elastic, getRelevantRcn, postgres

from prettytable import PrettyTable
from flask import json
import os

#
import requests

# postgres = 'postgr://postgres:example@localhost:5432/postgres'

templateId = "multimatch_query"
k = 10

template = {
    'id': templateId,
    'template': {
        'inline': getQuery('{{query_string}}', '')
        }
    }

metric_precision = {
      "precision": {
        "k" : k,
        "relevant_rating_threshold": 1,
        "ignore_unlabeled": "false"
      }
}

metric_mrr = {
    "mean_reciprocal_rank": {
        "k" : k,
        "relevant_rating_threshold" : 1
    }
}

metric_dcg = {
    "dcg": {
        "k" : k,
        "normalize": "false"
    }
}

metric_err =  {
    "expected_reciprocal_rank": {
        "maximum_relevance" : 3,
        "k" : k
    }
}

def getPayload(metric, topics):
    return {
        'templates': [template],
        'requests': topics,
        'metric': metric
    }

def getEvaluations():
    topics = getEvaluationTopics()



    topics_as_requests = []
    for topicId in topics.keys():
        # session.query(Relevant.rcn).filter(Relevant.topic_id == topicId).all()
        ratings = []
        relevantRcn = getRelevantRcn(topicId)
        for rcn in relevantRcn:
            rating = {'_index': 'reports', '_id': '{}'.format(rcn), 'rating': 1}

            ratings.append(rating)
        if ratings == []:
            continue
        # print(ratings)

        topic = {
            'id' : topicId,
            'ratings': ratings,
            'template_id': templateId,
            'params': {
                'query_string': topics[topicId]
            }
        }

        topics_as_requests.append(topic)


    # print(json.dumps(topics_as_requests , indent=4, sort_keys=False))
    elasticResponse = []
    for metric in [metric_precision, metric_dcg, metric_err, metric_mrr]:
        payload = getPayload(metric, topics_as_requests)
        # print(json.dumps(payload, indent=4, sort_keys=False))

        elasticEval = requests.get('http://{}:9200/reports/_rank_eval'.format(elastic), \
                                  data=json.dumps(payload), \
                                  headers= {'Content-Type': 'application/json'})
        elasticResponse.append(elasticEval.json())
    # print(elasticrequest.json())
    # print(json.dumps(elasticResponse, indent=4, sort_keys=False))
    return elasticResponse


def getEvaluationsAsDict():
    elasticResponse = getEvaluations()

    response = {
        'general value': {
            'precision at k': elasticResponse[0]['metric_score'],
            'discounted cumulative gain': elasticResponse[1]['metric_score'],
            'expected reciprocal rank': elasticResponse[2]['metric_score'],
            'mean reciprocal rank': elasticResponse[3]['metric_score']
        },
        'topics': []
    }

    for topic in elasticResponse[0]['details']:
        metricsForTopic = {
            'precision at k': elasticResponse[0]['details'][topic]['metric_score'],
            'discounted cumulative gain': elasticResponse[1]['details'][topic]['metric_score'],
            'expected reciprocal rank': elasticResponse[2]['details'][topic]['metric_score'],
            'mean reciprocal rank': elasticResponse[3]['details'][topic]['metric_score']
        }
        response['topics'].append({
            'topic_id': topic,
            'metrics': metricsForTopic
        })

    return response

def printEvalAsTable():
    elasticResponse = getEvaluations()
    table = PrettyTable()
    table.field_names = ['topic', 'precision at k', 'discounted cumulative gain', 'expected reciprocal rank', 'mean reciprocal rank', 'recall']


    mean_recall = 0
    for topic in elasticResponse[0]['details']:
        row = [int(topic.replace('t', ''))]
        row += ([elasticResponse[x]['details'][topic]['metric_score'] for x in range(4)])
        recall = (elasticResponse[0]['details'][topic]['metric_score'] * 10) / len(getRelevantRcn(topic))
        row.append(recall)
        table.add_row(row)
        mean_recall += recall

    table.sortby = 'topic'

    print(table)

    generalTable = PrettyTable()
    generalTable.field_names = ['topic', 'precision at k', 'discounted cumulative gain', 'expected reciprocal rank', 'mean reciprocal rank', 'recall']
    generalTable.add_row(['general value', elasticResponse[0]['metric_score'],
                   elasticResponse[1]['metric_score'],
                   elasticResponse[2]['metric_score'],
                   elasticResponse[3]['metric_score'],
                   mean_recall/50])

    print(generalTable)


def getEvalAsCSV():
    elasticResponse = getEvaluations()
    for i in range(50):
        topic = 't{}'.format(i+1)
        if topic in elasticResponse[0]['details'].keys():
            recall = (elasticResponse[0]['details'][topic]['metric_score'] * 10) / len(getRelevantRcn(topic))
            print('{}, {}, {}'.format(topic,elasticResponse[0]['details'][topic]['metric_score'], recall ))
        else:
            print('{}, {}, {}'.format(topic, 0, 0 ))


# getEvalAsCSV()


def exportRelevants():
    relevanceAsJson = {}
    for topic in getEvaluationTopics():
        relevanceAsJson['id'] = topic
        relevanceAsJson['relevant'] = getRelevantRcn(topic)

        with open('/app/Feedback/{}.json'.format(topic), 'w') as outfile:
            json.dump(relevanceAsJson, outfile, indent=4, sort_keys=False)

        print(json.dumps(relevanceAsJson, indent=4, sort_keys=False))



def importRelevants(body):
    # print(body)
    engine = create_engine(postgres)
    Base = declarative_base()
    Session = sessionmaker(bind=engine)
    session = Session()

    class Relevant(Base):
        __tablename__ = 'relevant'
        topic_id = Column(String, primary_key=True, nullable=False)
        rcn = Column(Integer, primary_key=True, nullable=False)
    #
    # class Evaluation(Base):
    #     __tablename__ = 'evaluations'
    #     session = Column(Integer, primary_key=True, nullable=False)
    #     version = Column(Integer)
    #     topic_id = Column(String, primary_key=True, nullable=False)
    #     rcn = Column(Integer, primary_key=True, nullable=False)

    topic = body['id']
    # print('topic:')
    # print(topic)
    existingRelevantRcn = getRelevantRcn(topic)
    # print('existing:')
    # print(existingRelevantRcn)
    for rcn in body['relevant']:
        if rcn not in existingRelevantRcn:
            newRelevantRcn = Relevant()
            newRelevantRcn.rcn = rcn
            newRelevantRcn.topic_id = topic
            # print(newRelevantRcn.rcn, newRelevantRcn.topic_id)
            session.add(newRelevantRcn)
        session.commit()
        session.close()

    relevantsAfterAdding = getRelevantRcn(topic)
    #### test-environment
    # with open('Feedback/{}.json'.format(topic), 'w') as outfile:
    ####deploy environment
    with open('/app/Feedback/{}.json'.format(topic), 'w') as outfile:
        json.dump({'id': topic, 'relevant': relevantsAfterAdding}, outfile, indent=4, sort_keys=False)