import os

from flask import json
from sqlalchemy import create_engine, Column, String, Integer
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker


##### deploy environment
elastic = 'elastic01.cordis_search_reesearchnet'
postgres = 'postgresql://postgres:example@postgres01.cordis_search_reesearchnet:5432/postgres'
topicspath = 'topicsList.json'  # deploy environment

##### test environment
# elastic = 'localhost'
# postgres = 'postgresql://postgres:example@localhost:5432/postgres'
# topicspath = os.path.join(os.getcwd(), 'topicsList.json')


def getEvaluationTopics():
    # path = os.path.join(os.getcwd(), 'topicsList.json')
    #######test environment
    # path = os.path.join(os.getcwd(), 'app/topicsList.json')
    #######deploy environment
    # path = 'topicsList.json' # deploy environment
    topicDict = {}
    with open(topicspath, 'r') as file:
        topics = json.load(file)
        for topic in topics:
            topicDict[topic['topic_id']] = topic['query']
    return topicDict


def getQuery(queryTerm, param):
    query = {
            "query": {
                "multi_match": {
                    "query": queryTerm,
                    "fields": ["title", "summary"]
                }
            }
    }
    if param == 'high':

        query['highlight'] = {
                "fields":  [
                { "title": {} },
                { "summary": {} }
            ]
        }
        return query
    else:
        return query



def getRelevantRcn(topicId):
    engine = create_engine(postgres)
    Base = declarative_base()
    Session = sessionmaker(bind=engine)
    session = Session()

    class Relevant(Base):
        __tablename__ = 'relevant'
        topic_id = Column(String, primary_key=True, nullable=False)
        rcn = Column(Integer, primary_key=True, nullable=False)

    relevants = [i[0] for i in session.query(Relevant.rcn).filter(Relevant.topic_id == topicId).all()]
    session.close()

    return relevants