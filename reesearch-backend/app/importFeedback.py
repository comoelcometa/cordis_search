from evaluation import importRelevants
from flask import json
import os

for i in range(50):
    # path = '/home/comoelcometa/Cordis_Search/cordis_search/reesearch-backend/app/Feedback'
    path = '/app/Feedback/t{}.json'.format(i+1)

    with open(path, 'r') as file:
        topic = json.load(file)
        importRelevants(topic)