from flask import Flask
from flask import abort
from flask import request
from flask import jsonify
from flask_cors import CORS

from elasticsearch import Elasticsearch
from elasticsearch.exceptions import ConnectionError

# import sqlalchemy as db
from sqlalchemy import create_engine
from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

from evaluation import getEvaluationsAsDict, importRelevants

from resources import getEvaluationTopics, postgres, getQuery, elastic

app = Flask(__name__)
CORS(app)
# CORS(app, resources={r"/api/*": {"origins": "*"}})

# postgres = 'postgresql://postgres:example@postgres01.cordis_search_reesearchnet:5432/postgres'
# elastic = 'elastic01.cordis_search_reesearchnet'


@app.route("/")
def hello():
    return "Hello World from Flask in a uWSGI Nginx Docker container with \
   Python 3.7 (from the example template)"

@app.route('/search',methods = ['GET'])
# @cross_origin(allow_headers=['Content-Type'])
def search():
    if request.method == 'GET':
        #paramQuery = request.args.get('query')

        if ('query' in request.args):
            paramQuery = request.args.get('query')
            # print(paramQuery)
            body = getQuery(paramQuery, 'high')
    #         body["highlight"] = {
    #         "fields":  [
    #         { "title": {} },
    #         { "summary": {} }
    #     ]
    # }
    #         # print(body)
            print(body)

            try:
                # es = Elasticsearch()
                es = Elasticsearch([{'host': elastic, 'port': 9200}])
                respondElastic = es.search(index="reports", body=body)
                # es.
                # print(respondElastic)
                # print(respondElastic['hits']['total']['value'])
            except ConnectionError:
                print('connection error')
                abort(500)
                return
            #
            # projectIds = []
            # for item in respondElastic['hits']['hits']:
            #     projectIds.append(int(item['_id']))
            #     projectIds.append(item['_source']['project'])


            # pre-configure database connection
            engine = create_engine(postgres)
            Base = declarative_base()
            Session = sessionmaker(bind=engine)
            session = Session()

            # setup table structure
            class Reports(Base):
                __tablename__ = 'reports'
                rcn = Column(Integer, primary_key =  True)
                title = Column(String)
                summary = Column(String)
                workperformed = Column(String)
                result = Column(String)
                projectid = Column(Integer)
                acronym = Column(String)

            class Organisations(Base):
                __tablename__ = 'organisations'
                rcn = Column(Integer, primary_key =  True)
                orgaid = Column(Integer)
                orgarole = Column(String)
                organame = Column(String)
                contribution = Column(Integer)

            # bind query to database objects
            # query = session.query(Reports).filter(Reports.rcn.in_(projectIds)).all()

            results = []
            projectIds = []
            index = 0
            for item in respondElastic['hits']['hits']:
                projectId = item['_source']['project']
                query = session.query(Reports).filter(Reports.rcn == projectId).all()
                queryOrganisations = session.query(Organisations).filter(Organisations.rcn == projectId).all()
                
                organisations = []
                if (queryOrganisations):
                    for organisation in queryOrganisations:
                        organisations.append({'name':organisation.organame})
                if (query):
                    results.append({'index':index,'projectId':query[0].rcn,'title':query[0].title, 'summary':query[0].summary, 'acronym':query[0].acronym, 'organisations':organisations}) #, 'highlights':item['highlight']['summary']})
                
                index += 1

            data = {'results': results}
            session.close()
            # print(index)
            # print(data)
            
        else:
            abort(400)
            return

        return jsonify(data)
    else:
        abort(404)
        return

@app.route('/topics',methods = ['GET'])
def getTopics():
    topicDict = getEvaluationTopics()
    #print(getEvaluationTopics())

    
    # print(json.dumps(getEvaluationTopics()))
    topics = []
    for topic in sorted(topicDict):
        topics.append({'topic':topic, 'query':topicDict[topic]})
        #print(topic + topicDict[topic])

    payLoad = {'topics':topics}

    return jsonify(payLoad)

@app.route('/eval', methods=['GET'])
def getEvaluations():
    return jsonify(getEvaluationsAsDict())

@app.route('/eval', methods=['PUT'])
def giveFeedback():
    body = request.get_json()
    print(body)

    engine = create_engine(postgres)
    Base = declarative_base()
    Session = sessionmaker(bind=engine)
    session = Session()

    importRelevants(body)

    class Evaluation(Base):
        __tablename__= 'evaluations'
        session = Column(Integer, primary_key=True, nullable=False)
        version = Column(Integer)
        topic_id = Column(String, primary_key=True, nullable=False)
        rcn = Column(Integer, primary_key=True, nullable=False)

    systSession = max([i[0] for i in session.query(Evaluation.session).filter().all()]) +1
    topic = body['id']

    # print(existingRelevantRcn)
    for rcn in body['relevant']:
        newEvalInfo = Evaluation()
        newEvalInfo.session =  systSession
        newEvalInfo.topic_id = topic
        newEvalInfo.rcn = rcn
        # TODO: implement system versioning an getting sys version
        newEvalInfo.version = 1

        session.add(newEvalInfo)
        session.commit()
    session.close()

    # exportRelevants()

    return jsonify()


if __name__ == "__main__":
    # Only for debugging while developing
    app.run(host='0.0.0.0', debug=True, port=5000)
