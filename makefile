projectname = reesearch
db_pw = example

#cordis_search_reesearchnet

# PROJECT_DIR=/home/comoelcometa/Cordis_Search/cordis_search
# PROJECT_DIR=/home/chrissi-mint/TEMP/git/backup/cordis_search

NETWORK_NAME=cordis_search_reesearchnet

PROJECT_DIR=$(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))

clean:
	docker-compose stop
	docker container prune
	docker image prune
	docker image rm reesearch-frontend
	docker image rm reesearch-backend
	docker network rm $(NETWORK_NAME)

build:
	npm --prefix ./$(projectname)-frontend/ install ./$(projectname)-frontend/
	npm run build --prefix ./$(projectname)-frontend/
	docker build -t $(projectname)-frontend:latest --rm ./$(projectname)-frontend/
	docker build -t $(projectname)-backend:latest --rm ./$(projectname)-backend/

deploy: ; pip3 install -r $(PROJECT_DIR)/reesearch-etl/requirements.txt
	docker network ls|grep $(NETWORK_NAME) > /dev/null || docker network create --driver "bridge" --ipam-driver "default" --subnet "172.16.238.0/24" --gateway "172.16.238.1" $(NETWORK_NAME)
	docker-compose up -d
	#docker run --rm -v $(PROJECT_DIR):/flyway/sql boxfuse/flyway -url=jdbc:postgresql://postgres01.cordis_search_reesearchnet:5432/postgres -user=postgres -password=example -connectRetries=5 migrate
	docker run --rm -v $(PROJECT_DIR)/sql:/flyway/sql --network="$(NETWORK_NAME)" boxfuse/flyway -url=jdbc:postgresql://postgres01.$(NETWORK_NAME):5432/postgres -user=postgres -password=example -connectRetries=5 migrate
#	docker exec reesearch-tracking bash -c "grep -q '0.0.0.0:9400' '/var/www/html/config/config.ini.php'\
#	 || echo "trusted_hosts[] = \"0.0.0.0:9400\"" >> /var/www/html/config/config.ini.php"
#	docker cp ./reesearch-tracking/config.ini.php reesearch-tracking:/var/www/html/config/

preprocess_data: ; python3 $(PROJECT_DIR)/reesearch-etl/preprocessData.py

import:	; docker exec reesearch-backend python3 importFeedback.py ; docker exec elastic01 curl -X DELETE "localhost:9200/reports" ; python3 $(PROJECT_DIR)/reesearch-etl/indexData.py 

export: ; docker cp reesearch-backend:/app/Feedback $(PROJECT_DIR)/reesearch-backend/app

evaluate: ; docker exec reesearch-backend python3 printEvalAsTable.py

delete: ; docker exec elastic01 curl -X DELETE "localhost:9200/reports"

csv: ; docker exec reesearch-backend python3 getEvalAsCSV.py

